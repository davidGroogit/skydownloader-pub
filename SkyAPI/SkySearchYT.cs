﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using YoutubeSearch;

namespace SkyAPI
{
    public class SkySearchYT
    {
        private string Query { get; set; }
        private int QueryPageCount = 2;//{ get; set; }
        public VideoSearch Items { get; set; }
        public List<VideoInformation> SearchResults { get; set; }

        /********************************************************************************
         * Description:     Constructor for the Youtube Search class                    *
         * Parameters :     string  Query sent from input                               *     
         *                  int     Page Count sent from Combo Box                      *
         ********************************************************************************/
        public SkySearchYT(string Query/*, int QueryPageCount*/)
        {
            SearchResults = new List<VideoInformation>();
            Items = new VideoSearch();
            this.Query = Query;
            PopulateList();
        }

        /********************************************************************************
         * Description:     Populates the SearchResults list with song information      *
         * Parameters :     N/A                                                         *                                
         ********************************************************************************/
        private void PopulateList()
        {
            foreach(var item in Items.SearchQuery(this.Query, this.QueryPageCount))
            {
                SearchResults.Add(item);
            }
        }

        public Task<List<VideoInformation>> SearchResultsAsync()
        {
            List<VideoInformation> temp = new List<VideoInformation>();
            foreach (var item in Items.SearchQuery(this.Query, this.QueryPageCount))
            {
                temp.Add(item);
            }

            return Task.FromResult(temp);
        }
    }
}
