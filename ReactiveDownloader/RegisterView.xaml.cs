﻿using ReactiveDownloader.models;
using ReactiveUI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ReactiveDownloader
{
    /// <summary>
    /// Interaction logic for RegisterView.xaml
    /// </summary>
    public partial class RegisterView : ReactiveWindow<RegisterViewModel>
    {
        public RegisterView()
        {
            InitializeComponent();

            Application.Current.Windows[0].Close();
            ViewModel = new RegisterViewModel();

            this.WhenActivated(disposableRegistration =>
            {

                this.Bind(ViewModel,
                    viewModel => viewModel.Username,
                    view => view.txtUsername.Text)
                    .DisposeWith(disposableRegistration);

                txtPassword
                          .Events()
                          .PasswordChanged
                          .Select(_ => txtPassword.Password)
                          .Subscribe(x => ViewModel.Password = x)
                          .DisposeWith(disposableRegistration);

                this.Bind(ViewModel,
                    viewModel => viewModel.Email,
                    view => view.txtEmail.Text)
                    .DisposeWith(disposableRegistration);

                this.BindCommand(ViewModel,
                    viewModel => viewModel.QuitApp,
                    view => view.btnQuit)
                    .DisposeWith(disposableRegistration);

                this.BindCommand(ViewModel,
                    viewModel => viewModel.OpenLogin,
                    view => view.btnLogin)
                    .DisposeWith(disposableRegistration);

                this.BindCommand(ViewModel,
                    viewModel => viewModel.RegisterUser,
                    view => view.btnRegister)
                    .DisposeWith(disposableRegistration);

            });

        }

        private void ReactiveWindow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
