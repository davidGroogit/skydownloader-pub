﻿using MediaToolkit;
using MediaToolkit.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Controls;
using VideoLibrary;
using YoutubeSearch;

namespace ReactiveDownloader
{

    public class DownloadManager
    {
        public void Download(VideoInformation videoInfo, bool mp3, bool mp4)
        {
            var youtube = YouTube.Default;
            var video = youtube.GetVideo(videoInfo.Url);
            var musicPath = Environment.GetFolderPath(Environment.SpecialFolder.MyMusic) + "\\" + video.FullName;
            var videoPath = Environment.GetFolderPath(Environment.SpecialFolder.MyVideos) + "\\" + video.FullName;
            
            if (mp3 && mp4)
            {
                Debug.Print("Downloading");
                Thread downloadThread;
                downloadThread = new Thread(() => DownloadBoth(musicPath, videoPath, video));
                downloadThread.Start();
            }
            else if (!mp3 && mp4)
            {
                Debug.Print("Downloading");
                Thread downloadThread2;
                downloadThread2 = new Thread(() => DownloadMP4(videoPath, video));
                downloadThread2.Start();
            }
            else if (!mp4 && mp3)
            {
                Debug.Print("Downloading");
                Thread downloadThread3;
                downloadThread3 = new Thread(() => DownloadMP3(musicPath, videoPath, video));
                downloadThread3.Start();
            }
        }

        private void DownloadBoth(string musicPath, string videoPath, YouTubeVideo video)
        {
            DownloadMP4(videoPath, video);

            var inputFile = new MediaFile { Filename = videoPath };
            var outputFile = new MediaFile { Filename = $"{musicPath}.mp3" };

            using (var engine = new Engine())
            {
                engine.GetMetadata(inputFile);
                engine.Convert(inputFile, outputFile);
            }
        }

        private void DownloadMP4(string videoPath, YouTubeVideo video)
        {
            try
            {
                File.WriteAllBytes(videoPath, video.GetBytes());
            }
            catch (Exception exc)
            {
                Debug.Print(exc.Message);
            }

        }

        private void DownloadMP3(string musicPath, string videoPath, YouTubeVideo video)
        {
            DownloadBoth(musicPath, videoPath, video);
            if (File.Exists(videoPath))
            {
                File.Delete(videoPath);
            }
        }
    }
}
