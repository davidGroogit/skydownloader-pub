﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ReactiveUI;
using ReactiveDownloader.models;
using System.Reactive.Disposables;

namespace ReactiveDownloader.controls
{
    /// <summary>
    /// Interaction logic for VideoListViewItem.xaml
    /// </summary>
    public partial class VideoListViewItem : ReactiveUserControl<VideoViewModel>
    {
        public VideoListViewItem()
        {
            InitializeComponent();


            this.WhenActivated(disposableRegistration =>
            {

                this.OneWayBind(ViewModel,
                    viewModel => viewModel.Title,
                    view => view.songTitle.Content)
                    .DisposeWith(disposableRegistration);

                this.OneWayBind(ViewModel,
                    viewModel => viewModel.Duration,
                    view => view.songDuration.Content)
                    .DisposeWith(disposableRegistration);

                //this.Bind(ViewModel,
                //    viewModel => viewModel.IsDownloading,
                //    view => view.pbSearching.IsVisible)
                //    .DisposeWith(disposableRegistration);

                this.BindCommand(ViewModel,
                    viewModel => viewModel.DownloadSong,
                    view => view.btnDownload)
                    .DisposeWith(disposableRegistration);
            });
        }
    }
}
