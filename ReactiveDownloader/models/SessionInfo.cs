﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ReactiveDownloader.models
{
    /// <summary>
    /// Current session information
    /// </summary>
    public class SessionInfo
    {
        /// <summary>
        /// User's unique ID
        /// </summary>
        public static int ID { get; set; }

        /// <summary>
        /// User's unique Email address
        /// </summary>
        public static string Email { get; set; }

        public static readonly HttpClient client = new HttpClient();

        public static bool FirstWindow = true;
    }
}
