﻿using ReactiveDownloader.repos;
using ReactiveUI;
using Splat;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ReactiveDownloader.models
{
    public class RegisterViewModel : ReactiveObject
    {
        RegisterRepository registerRepo;
        private int response;

        public string Username
        {
            get => _username;
            set => this.RaiseAndSetIfChanged(ref _username, value);
        }
        private string _username;

        public string Password
        {
            get => _password;
            set => this.RaiseAndSetIfChanged(ref _password, value);
        }
        private string _password;

        public string Email
        {
            get => _email;
            set => this.RaiseAndSetIfChanged(ref _email, value);
        }
        private string _email;

        /// <summary>
        /// <c>IsLoggingIn</c> is a <c>ObservableAsPropertyHelper</c> that keeps track of the <c>LoginUser</c> command while it is executing.
        /// </summary>
        public bool IsRegistering { get => _isRegistering.Value; }
        readonly ObservableAsPropertyHelper<bool> _isRegistering;


        /// <summary>
        /// <c>Response</c> is a public propertiy used to receive a callback from the <c>LoginUser</c> command.
        /// <list type="bullet">
        /// <listheader>
        ///     <term>Response Codes</term>
        ///     <description>Possible response codes received as callback.</description>
        /// </listheader>
        /// <item>
        ///     <term>202</term>
        ///     <description>Success code. Username found and hashed password matches.</description>
        /// </item>
        /// <item>
        ///     <term>401</term>
        ///     <description>Error code. Username not found or password does not match.</description>
        /// </item>
        /// </list>
        /// </summary>
        public int Response { get => response; set => response = value; }

        /// <summary>
        /// Callback is a relay property of <c>LoginViewModel</c> that links to the <c>LoginUser</c> ReactiveCommand.
        /// <returns>Returns a string based on the callback code sent from the command.</returns>
        /// </summary>
        public string Callback
        {
            get => _callback;
            set => this.RaiseAndSetIfChanged(ref _callback, value);
        }
        private string _callback;


        /// <summary>
        /// <c>ErrorResponse</c> is the propertie that binds to <c>Callback</c>
        /// </summary>
        public string ErrorResponse => _errorResponse.Value;
        private readonly ObservableAsPropertyHelper<string> _errorResponse;

        public RegisterViewModel()
        {
            _errorResponse = this
                .WhenAnyValue(x => x.Callback)
                .Throttle(TimeSpan.FromMilliseconds(600))
                .Select(callback => callback?.Trim())
                .DistinctUntilChanged()
                .Where(callback => !string.IsNullOrWhiteSpace(callback))
                .SelectMany(CallbackMethod)
                .ObserveOn(RxApp.MainThreadScheduler)
                .ToProperty(this, x => x.ErrorResponse);

            _errorResponse.ThrownExceptions.Subscribe(error => { });


            RegisterUser = ReactiveCommand.CreateFromObservable(RegisterUserImplementation);
            RegisterUser.IsExecuting.ToProperty(this, x => x.IsRegistering, out _isRegistering);
            RegisterUser.ThrownExceptions.Subscribe(exc => this.Log().ErrorException("Registration failed", exc));

            OpenLogin = ReactiveCommand.CreateFromObservable(OpenLoginImplementation);
            QuitApp = ReactiveCommand.Create(() => { Application.Current.Shutdown(); });
        }

        public ReactiveCommand<Unit, Unit> QuitApp { get; }
        public ReactiveCommand<Unit, string> OpenLogin { get; }
        public ReactiveCommand<Unit, string> RegisterUser { get; }



        public IObservable<string> OpenLoginImplementation()
        {
            return Observable.Start(() =>
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    SessionInfo.FirstWindow = true;
                    LoginWindow view = new LoginWindow();
                    view.Show();
                });
                return "test";
            });
        }


        public IObservable<string> RegisterUserImplementation()
        {
            registerRepo = new RegisterRepository(Username, Email, Password);


            return Observable.Start(() =>
            {
                this.Response = registerRepo.Register().Result;
                Debug.Print("Response:" + this.Response);

                return "test";
            });
        }


        /// <summary>
        /// Simple binding command for async connection to the views.
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        private async Task<string> CallbackMethod(string callback)
        {
            return _callback;
        }
    }
}
