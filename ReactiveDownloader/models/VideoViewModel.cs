﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using ReactiveUI;
using Splat;
using YoutubeSearch;

namespace ReactiveDownloader.models
{
    /// <summary>
    /// Video ViewModel bound to the <c>VideoListViewItem</c>
    /// </summary>
    public class VideoViewModel : ReactiveObject
    {
        private readonly VideoInformation _videoInfo;
        private DownloadManager manager;
        readonly ObservableAsPropertyHelper<bool> _isDownloading;

        /// <summary>
        /// Download MP3
        /// </summary>
        public bool MP3
        {
            get => _mp3;
            set => this.RaiseAndSetIfChanged(ref _mp3, value);
        }
        private bool _mp3;

        /// <summary>
        /// Download MP4
        /// </summary>
        public bool MP4
        {
            get => _mp4;
            set => this.RaiseAndSetIfChanged(ref _mp4, value);
        }
        private bool _mp4;


        /// <summary>
        /// VideoViewModel contructor
        /// </summary>
        /// <param name="videoInfo"></param>
        /// <param name="mp3"></param>
        /// <param name="mp4"></param>
        public VideoViewModel(VideoInformation videoInfo, bool mp3, bool mp4)
        {
            _videoInfo = videoInfo;
            manager = new DownloadManager();

            _mp3 = mp3;
            _mp4 = mp4;
            
            DownloadSong = ReactiveCommand.CreateFromObservable(DownloadImplementation);
            DownloadSong.IsExecuting.ToProperty(this, x => x.IsDownloading, out _isDownloading);
            DownloadSong.ThrownExceptions.Subscribe(exc => this.Log().ErrorException("Downloading Song Failed.", exc));
        }

        #region Video Information
        /// <summary>
        /// Song Title
        /// </summary>
        public string Title => _videoInfo.Title;

        /// <summary>
        /// Song thumbnail URL
        /// </summary>
        public string Thumbnail => _videoInfo.Thumbnail;

        /// <summary>
        /// Song author (On youtube)
        /// </summary>
        public string Author => _videoInfo.Author;

        /// <summary>
        /// Video Description
        /// </summary>
        public string Description => _videoInfo.Description;

        /// <summary>
        /// Video duration
        /// </summary>
        public string Duration => _videoInfo.Duration;

        /// <summary>
        /// Video view count
        /// </summary>
        public string ViewCount => _videoInfo.ViewCount;
        #endregion

        /// <summary>
        /// Keeps track of if the song is downloading and/or converting
        /// </summary>
        public bool IsDownloading { get => _isDownloading.Value; }

        /// <summary>
        /// Command to start the song download
        /// </summary>
        public ReactiveCommand<Unit, Unit> DownloadSong { get; }

        /// <summary>
        /// Reactive command's implementation
        /// </summary>
        /// <returns></returns>
        public IObservable<Unit> DownloadImplementation()
        {
            return Observable.Start(() => manager.Download(_videoInfo, MP3, MP4));
        }

    }
}
