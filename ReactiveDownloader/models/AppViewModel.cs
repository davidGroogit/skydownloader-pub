﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using ReactiveUI;
using SkyAPI;
using Splat;
using YoutubeSearch;


namespace ReactiveDownloader.models
{
    /// <summary>
    /// Main Window view model. Handles the logic from the <c>MainWindow</c> view.
    /// </summary>
    public class AppViewModel : ReactiveObject
    {
        #region Properties
        
        /// <summary>
        /// Property bound to the search text box in the <c>MainWindow</c> view.
        /// </summary>
        public string SearchTerm
        {
            get => _searchTerm;
            set => this.RaiseAndSetIfChanged(ref _searchTerm, value);
        }
        private string _searchTerm;

        /// <summary>
        /// Property bound to the MP3 checkbox in the <c>MainWindow</c> view
        /// </summary>
        public bool IsCheckedMp3
        {
            get => _isCheckedMp3;
            set => this.RaiseAndSetIfChanged(ref _isCheckedMp3, value);
        }
        private bool _isCheckedMp3;

        /// <summary>
        /// Property bound to the MP4 checkbox in the <c>MainWindow</c> view
        /// </summary>
        public bool IsCheckedMp4
        {
            get => _isCheckedMp4;
            set => this.RaiseAndSetIfChanged(ref _isCheckedMp4, value);
        }
        private bool _isCheckedMp4;
        
        /// <summary>
        /// Property bound to the ListView's Visibility in the <c>MainWindow</c> view
        /// </summary>
        public bool IsAvailable => _isAvailable.Value;
        private readonly ObservableAsPropertyHelper<bool> _isAvailable;

        /// <summary>
        /// Property bound to the return value of <c>SearchVideoYoutube</c> method.
        /// </summary>
        public IEnumerable<VideoViewModel> SearchResults => _searchResults.Value;
        private readonly ObservableAsPropertyHelper<IEnumerable<VideoViewModel>> _searchResults;

        /// <summary>
        /// Property that binds to when the ListView in <c>MainWindow</c> view is being populated.
        /// </summary>
        public bool IsSearching { get => _isSearching.Value; }
        readonly ObservableAsPropertyHelper<bool> _isSearching;

        #endregion

        /// <summary>
        /// Constructor for the <c>AppViewModel</c>
        /// </summary>
        public AppViewModel()
        {

            _searchResults = this
                .WhenAnyValue(x => x.SearchTerm)
                .Throttle(TimeSpan.FromMilliseconds(600))
                .Select(term => term?.Trim())
                .DistinctUntilChanged()
                .Where(term => !string.IsNullOrWhiteSpace(term))
                .SelectMany(SearchVideoYoutube)
                .ObserveOn(RxApp.MainThreadScheduler)
                .ToProperty(this, x => x.SearchResults);

            _searchResults.ThrownExceptions.Subscribe(error => { });


            _isAvailable = this
                .WhenAnyValue(x => x.SearchResults)
                .Select(searchResults => searchResults != null)
                .ToProperty(this, x => x.IsAvailable);

            QuitApp = ReactiveCommand.Create(() => { Application.Current.Shutdown(); });

            SearchSong = ReactiveCommand.Create(() => { Application.Current.Shutdown(); });
            SearchSong.IsExecuting.ToProperty(this, x => x.IsSearching, out _isSearching);
        }

        #region Private Methods
        private async Task<IEnumerable<VideoViewModel>> SearchVideoYoutube(string term)
        {
            SkySearchYT search = new SkySearchYT(term);
            List<VideoInformation> videos = await search.SearchResultsAsync();


            return videos.Select(x => new VideoViewModel(x, _isCheckedMp3, _isCheckedMp4));
        }
        #endregion

        #region Commands
        /// <summary>
        /// Command to shutdown application.
        /// </summary>
        public ReactiveCommand<Unit, Unit> QuitApp { get; }

        /// <summary>
        /// Command to start the search for populating the ListView in <c>MainWindow</c>
        /// </summary>
        public ReactiveCommand<Unit, Unit> SearchSong { get; }
        #endregion
    }
}
