﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive;
using System.Reactive.Linq;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using ReactiveDownloader.repos;
using ReactiveUI;
using Splat;

namespace ReactiveDownloader.models
{
    /// <summary>
    /// <c>LoginViewModel</c> if the ViewModel for the login view. Inherits off of <c>ReactiveObject</c>
    /// </summary>
    public class LoginViewModel : ReactiveObject
    {
        #region Properties
        /// <summary>
        /// <c>Username</c> is a bound property of <c>LoginViewModel</c> that links to the LoginView <c>Username Textbox</c>
        /// </summary>
        public string Username
        {
            get => _username;
            set => this.RaiseAndSetIfChanged(ref _username, value);
        }
        private string _username;

        /// <summary>
        /// <c>Password</c> is a bound property of <code>LoginViewModel</code> that links to the LoginView <c>Password Textbox</c>
        /// </summary>
        public string Password
        {
            get => _password;
            set => this.RaiseAndSetIfChanged(ref _password, value);
        }
        private string _password;

        /// <summary>
        /// Callback is a relay property of <c>LoginViewModel</c> that links to the <c>LoginUser</c> ReactiveCommand.
        /// <returns>Returns a string based on the callback code sent from the command.</returns>
        /// </summary>
        public string Callback
        {
            get => _callback;
            set => this.RaiseAndSetIfChanged(ref _callback, value);
        }
        private string _callback;

        /// <summary>
        /// <c>IsLoggingIn</c> is a <c>ObservableAsPropertyHelper</c> that keeps track of the <c>LoginUser</c> command while it is executing.
        /// </summary>
        public bool IsLoggingIn { get => _isLoggingIn.Value; }
        readonly ObservableAsPropertyHelper<bool> _isLoggingIn;

        /// <summary>
        /// <c>Response</c> is a public propertiy used to receive a callback from the <c>LoginUser</c> command.
        /// <list type="bullet">
        /// <listheader>
        ///     <term>Response Codes</term>
        ///     <description>Possible response codes received as callback.</description>
        /// </listheader>
        /// <item>
        ///     <term>202</term>
        ///     <description>Success code. Username found and hashed password matches.</description>
        /// </item>
        /// <item>
        ///     <term>401</term>
        ///     <description>Error code. Username not found or password does not match.</description>
        /// </item>
        /// </list>
        /// </summary>
        public int Response { get => response; set => response = value; }
        
        /// <summary>
        /// <c>ErrorResponse</c> is the propertie that binds to <c>Callback</c>
        /// </summary>
        public string ErrorResponse => _errorResponse.Value;
        private readonly ObservableAsPropertyHelper<string> _errorResponse;
        #endregion

        LoginRepository repo;
        private int response;

        /// <summary>
        /// <c>Contructor</c> for the <c>LoginViewModel</c>
        /// </summary>
        public LoginViewModel()
        {
            _errorResponse = this
                .WhenAnyValue(x => x.Callback)
                .Throttle(TimeSpan.FromMilliseconds(600))
                .Select(callback => callback?.Trim())
                .DistinctUntilChanged()
                .Where(callback => !string.IsNullOrWhiteSpace(callback))
                .SelectMany(CallbackMethod)
                .ObserveOn(RxApp.MainThreadScheduler)
                .ToProperty(this, x => x.ErrorResponse);

            _errorResponse.ThrownExceptions.Subscribe(error => { });


            LoginUser = ReactiveCommand.CreateFromObservable(LoginImplementation);
            LoginUser.IsExecuting.ToProperty(this, x => x.IsLoggingIn, out _isLoggingIn);
            LoginUser.ThrownExceptions.Subscribe(exc => this.Log().ErrorException("Login failed", exc));

            OpenRegister = ReactiveCommand.CreateFromObservable(RegisterImplementation);
            LoginUser.ThrownExceptions.Subscribe(exc => this.Log().ErrorException("Register window opening failed", exc));

            QuitApp = ReactiveCommand.Create(() => { Application.Current.Shutdown(); });
        }

        /// <summary>
        /// <c>LoginUser</c> is a <c>ReactiveCommand</c> that binds to a view from the <c>LoginView</c>
        /// </summary>
        public ReactiveCommand<Unit, string> LoginUser { get; }

        /// <summary>
        /// Implementation for <c>LoginUser</c> which handles the logic of the command.
        /// </summary>
        /// <returns>Returns the callback message based on the response code.</returns>
        public IObservable<string> LoginImplementation()
        {
            repo = new LoginRepository(Username, Password);

            return Observable.Start(() =>
            {
                this.Response = repo.Login().Result;

                if (this.Response == Codes.UserDoesNotExist)
                {
                    //ErrorResponse = "The username of password you entered is incorrect.";
                    Callback = "The username or password you entered is incorrect.";

                }
                else if (this.Response == Codes.LoginSuccessful)
                {
                    Callback = "Logging in...";

                    Application.Current.Dispatcher.Invoke((Action)delegate
                    {
                        MainWindow window = new MainWindow();
                        window.Show();
                    });
                }

                return "test";
            });
        }

        public IObservable<string> RegisterImplementation()
        {
            return Observable.Start(() =>
            {
                Application.Current.Dispatcher.Invoke((Action)delegate
                {
                    RegisterView view = new RegisterView();
                    view.Show();
                });
                return "test";
            });
        }

        /// <summary>
        /// Simple binding command for async connection to the views.
        /// </summary>
        /// <param name="callback"></param>
        /// <returns></returns>
        private async Task<string> CallbackMethod(string callback)
        {
            return _callback;
        }

        /// <summary>
        /// Closes the current running application
        /// </summary>
        public ReactiveCommand<Unit, Unit> QuitApp { get; }

        public ReactiveCommand<Unit, string> OpenRegister { get; }
    }
}
