﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReactiveDownloader.models
{
    public class ResponseStatus
    {
        public string Message { get; set; }
        public int Status { get; set; }
    }
}
