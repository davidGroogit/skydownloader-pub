﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReactiveDownloader.models
{
    /// <summary>
    /// Callback codes for all backend API callss
    /// </summary>
    public class Codes
    {
        /// <summary>
        /// 401: Error Code. If the user does not exist in the database.
        /// </summary>
        public static int UserDoesNotExist
        {
            get => 401;
        }

        public static int UserIDAlreadyExists
        {
            get => 406;
        }

        public static int EmailAddressAlreadyExists
        {
            get => 407;
        }
        
        /// <summary>
        /// 401: Error Code. If the password does not match with the compared value.
        /// </summary>
        public static int PasswordDoesNotMatch
        {
            get => 401;
        }

        public static int RegisterPostFailed
        {
            get => 403;
        }

        public static int LoginPostFailed
        {
            get => 405;
        }

        public static int LoginSuccessful
        {
            get => 201;
        }

        /// <summary>
        /// 202: Success Code. Username and password matches.
        /// </summary>
        public static int PasswordDoesMatch
        {
            get => 202;
        }

        public static int RegisterSuccessful
        {
            get => 203;
        }
    }
}
