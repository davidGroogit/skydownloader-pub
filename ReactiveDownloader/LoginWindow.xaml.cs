﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ReactiveDownloader.models;
using ReactiveUI;

namespace ReactiveDownloader
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : ReactiveWindow<LoginViewModel>
    {
        /// <summary>
        /// Login View
        /// </summary>
        public LoginWindow()
        {
            InitializeComponent();

            

            for (int intCounter = Application.Current.Windows.Count - 1; intCounter >= 0; intCounter--)
            {
                if(Application.Current.Windows[intCounter].Uid == "RegisterView")
                {
                    Application.Current.Windows[intCounter].Close();
                }
            }



            ViewModel = new LoginViewModel();

            this.WhenActivated(disposableRegistration =>
            {
                #region Bindings
                this.Bind(ViewModel,
                    viewModel => viewModel.Username,
                    view => view.txtUsername.Text)
                    .DisposeWith(disposableRegistration);

                txtPassword
                          .Events()
                          .PasswordChanged
                          .Select(_ => txtPassword.Password)
                          .Subscribe(x => ViewModel.Password = x)
                          .DisposeWith(disposableRegistration);

                this.BindCommand(ViewModel,
                    viewModel => viewModel.LoginUser,
                    view => view.btnLogin)
                    .DisposeWith(disposableRegistration);

                this.Bind(ViewModel,
                    viewModel => viewModel.ErrorResponse,
                    view => view.lblResponse.Content)
                    .DisposeWith(disposableRegistration);

                this.BindCommand(ViewModel,
                    viewModel => viewModel.QuitApp,
                    view => view.btnQuit)
                    .DisposeWith(disposableRegistration);

                this.BindCommand(ViewModel,
                    viewModel => viewModel.OpenRegister,
                    view => view.btnRegister)
                    .DisposeWith(disposableRegistration);
                #endregion
            });
        }

        private void ReactiveWindow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}

