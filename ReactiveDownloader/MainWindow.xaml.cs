﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reactive.Disposables;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ReactiveDownloader.models;
using ReactiveUI;


namespace ReactiveDownloader
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : ReactiveWindow<AppViewModel>
    {
        public MainWindow()
        {
            InitializeComponent();

            ViewModel = new AppViewModel();

            Application.Current.Windows[0].Close();

            this.WhenActivated(disposableRegistration =>
            {

                //Bind my list if visible
                this.OneWayBind(ViewModel,
                viewModel => viewModel.IsAvailable,
                view => view.VideoList.Visibility)
                .DisposeWith(disposableRegistration);


                //Bind the SearchResults to ItemSource of list view
                this.OneWayBind(ViewModel,
                viewModel => viewModel.SearchResults,
                view => view.VideoList.ItemsSource)
                .DisposeWith(disposableRegistration);

            //Bind the search Query to my txtSearch control
            this.Bind(ViewModel,
                viewModel => viewModel.SearchTerm,
                view => view.txtSearch.Text)
                .DisposeWith(disposableRegistration);

            //Bind the mp3 check box
            this.Bind(ViewModel,
                viewModel => viewModel.IsCheckedMp3,
                view => view.checkMp3.IsChecked)
                .DisposeWith(disposableRegistration);

            //Bind the mp4 checkbox
            this.Bind(ViewModel,
                viewModel => viewModel.IsCheckedMp4,
                view => view.checkMp4.IsChecked)
                .DisposeWith(disposableRegistration);

            //Bind the quit button
            this.BindCommand(ViewModel,
                viewModel => viewModel.QuitApp,
                view => view.btnQuit)
                .DisposeWith(disposableRegistration);
            });
        }

        private void MainReactiveWindow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ChangedButton == MouseButton.Left)
                this.DragMove();
        }
    }
}
