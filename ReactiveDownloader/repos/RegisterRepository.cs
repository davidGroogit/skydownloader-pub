﻿using Newtonsoft.Json;
using ReactiveDownloader.models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace ReactiveDownloader.repos
{
    public class RegisterRepository
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }

        public ResponseStatus status;

        public RegisterRepository(string username,  string email, string password)
        {
            this.Username = username;
            this.Password = password;
            this.Email = email;
        }

        public async Task<int> Register()
        {
            Dictionary<string, string> values = new Dictionary<string, string>{
                { "uid", this.Username },
                { "pwd", this.Password },
                { "ema", this.Email }
            };

            var content = new FormUrlEncodedContent(values);
            try
            {
                HttpResponseMessage response = await SessionInfo.client.PostAsync("http://localhost:8000/api/users/create", content);
                var responseString = await response.Content.ReadAsStringAsync();
                status = JsonConvert.DeserializeObject<ResponseStatus>(responseString);

                return status.Status;
            }
            catch (Exception)
            {
                Debug.Print("Post failed.");
                return Codes.RegisterPostFailed;
            }
        }
    }
}
