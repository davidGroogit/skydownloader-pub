﻿using System;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;
using System.Data.Sql;
using ReactiveDownloader.models;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace ReactiveDownloader.repos
{
    /// <summary>
    /// Login logic
    /// </summary>
    public class LoginRepository
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public ResponseStatus status;

        /// <summary>
        /// Login constructor
        /// </summary>
        /// <param name="Username"></param>
        /// <param name="Password"></param>
        public LoginRepository(string Username, string Password)
        {
            this.Username = Username;
            this.Password = Password;
        }

        /// <summary>
        /// Main login logic
        /// </summary>
        /// <returns>Error code</returns>
        public async Task<int> Login()
        {
            Dictionary<string, string> values = new Dictionary<string, string>{
                { "uid", this.Username },
                { "pwd", this.Password }
            };

            var content = new FormUrlEncodedContent(values);
            try
            {
                HttpResponseMessage response = await SessionInfo.client.PostAsync("http://localhost:8000/api/users/login", content);
                var responseString = await response.Content.ReadAsStringAsync();

                status = JsonConvert.DeserializeObject<ResponseStatus>(responseString);

                return status.Status;
            }
            catch (Exception)
            {
                Debug.Print("Post failed.");
                return Codes.LoginPostFailed;
            }
        }

    }
}
