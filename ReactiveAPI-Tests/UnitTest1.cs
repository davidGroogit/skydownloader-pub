﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ReactiveDownloader;
using ReactiveDownloader.repos;

namespace ReactiveAPI_Tests
{
    [TestClass]
    public class UnitTest1
    {
        private TestContext testContextInstance;

        /// <summary>
        ///  Gets or sets the test context which provides
        ///  information about and functionality for the current test run.
        ///</summary>
        public TestContext TestContext
        {
            get { return testContextInstance; }
            set { testContextInstance = value; }
        }


        [TestMethod]
        public void RegistratorInstanciator()
        {
            string Username = "SkyPhase";
            string Password = "fightme.com";
            string email = "fightme@2am.com";
            RegisterRepository registerator = new RegisterRepository(Username, email, Password);
        }

        [TestMethod]
        public void TestRegistration()
        {
            string Username = "Jockie";
            string Password = "jockiepass";
            string email = "fightme@gmail.com";

            RegisterRepository registerator = new RegisterRepository(Username, email, Password);
            string registered = registerator.Register().Result.ToString();

            TestContext.WriteLine(registered);
        }

        [TestMethod]
        public void LoginInstanciator()
        {
            string username = "rosd02@hotmail.fr";
            string password = "noob123";
            LoginRepository login = new LoginRepository(username, password);
        }

        [TestMethod]
        public void TestLogin()
        {
            string username = "fightme@gmail.com";
            string password = "jockiepa";

            LoginRepository login = new LoginRepository(username, password);
            int loginResult = login.Login().Result;

            TestContext.WriteLine(loginResult.ToString());
        }
    }
}
